
var contenidoDiv = '';
var imparPar;
var activ;
ReadJson('/json/actividadesPM.json', 'saveActividades(data)');
ReadJson('/json/PoliticasMarketing.json', 'drawTimeLine(data)');

function saveActividades(data) {
    activ = data;
}

function ReadJson(archivo, funcion) {
    $.ajax({
        url: archivo,
        dataType: 'json',
        async: false,
        success: function (data) {
            // Manipula los datos del archivo JSON aquí
            eval(funcion);
        },
        error: function (xhr, status, error) {
            console.log('Error al cargar el archivo JSON');
        }
    });
}
function drawTimeLine(datos) {

    // Recorrer los elementos del JSON
    for (var i = 0; i < datos.length; i++) {
        var orden = datos[i].orden;
        var titulo = datos[i].titulo;
        var imagen = datos[i].imagen;
        var texto = datos[i].texto;
        var enlace = datos[i].enlace;
        var actividades = datos[i].actividades
        var avance = datos[i].avance
        var ac=datos[i].AC
        if (i % 2 == 0) { imparPar = "actividadeSl" } else { imparPar = "actividadeSr" };


        // Mostrar los datos en el div de contenido

        contenidoDiv += '<div class="timeline-box one-of-two">';
        contenidoDiv += '<div class="contenedore"><img class="mb-1-6 rounded mandaAncho" src="';
        contenidoDiv += 'imagenes/imagen' + (i + 1) + '.jpg';
        contenidoDiv += '"alt="..."></div>'
        contenidoDiv += '<div class=content>'
        contenidoDiv += '<h3 class=h4 mb-2 mb-md-3>' + titulo + '</h3>';
        contenidoDiv += '<p class=mb-0>' + texto + '</p>';
        contenidoDiv += '<div class="actividadeS ' + imparPar + '">';
        try {
            for (var j = 0; j < ac.length; j++) {
                for (var k = 0; k < activ.length; k++) {
                if(ac[j].act==activ[k].id)
                {

                    contenidoDiv += '<div><p class="mb-0"><a target="_blank" href="/actividad.html?actividad=' + activ[k].id+ '&rol=A">';
                      
                    
                    contenidoDiv += '<img style="width:24px;height:24px;" src="imagenes/iconos/';
                    
                    if(activ[k].tipo=='Simulación') {contenidoDiv+='Simulacion';}
                    else if(activ[k].tipo=='Desafío') {contenidoDiv+='Desafio';}
                    else if(activ[k].tipo=='Caso práctico') {contenidoDiv+='casopractico';}
                    else
                    {contenidoDiv += activ[k].tipo;}

                    contenidoDiv +='.svg"/>'
                    contenidoDiv += activ[k].tipo + '</p>';
                    contenidoDiv += '</a></div>';

                }
                console.log(ac[j].act);
                }
            }
        } catch {

        }
        //for (var j = 0; j < ac.length; j++) {
        //    console.log(ac[j].act);
        //}
        for (var j = 0; j < actividades.length; j++) {
            //actividades que tenga información
            if (actividades[j].enlace != '') {
                contenidoDiv += '<div><p class="mb-0"><a href="contenido/unidades/' + actividades[j].enlace + '.html">'
                contenidoDiv += '<img style="width:24px;height:24px;" src="imagenes/iconos/' + actividades[j].actividad + '.svg"/>'
                contenidoDiv += actividades[j].actividad + '</p>';
                contenidoDiv += '</a></div>';
            }
        }
        contenidoDiv += '<p class="mb-0" style="clear:both;padding-top:20px;">Avance ' + avance + ' %</p>';
        contenidoDiv += '</div>';
        contenidoDiv += '</div>';
        contenidoDiv += '<div class="year">' + orden + '</div>';
        contenidoDiv += '</div>';
        //alert(contenidoDiv);

    }
    document.getElementById('elementos').innerHTML += contenidoDiv

}


   

