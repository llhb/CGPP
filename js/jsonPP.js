(function ($) {
"use strict";

//lectura json
ReadJson('json/PoliticasMarketing.json', 'drawCarousel(data)');
ReadJson('json/actividades.json', 'saveActividades(data)');
ReadJson('json/actividadesPM.json', 'saveActividadesPM(data)');
ReadJson('json/unidades.json', 'drawActividades(data)');
})(jQuery);

var contenidoDiv = '';
var imparPar;
var activ;
var activPM;
var textoIE;


function drawCarousel(datos) {
	//contenidoDiv = '<div class="featured-carousel owl-carousel">';
	
	var UT2='';
	// Recorrer los elementos del JSON
	for (var i = 0; i < datos.length; i++) 
	{
		//if(UT2!=UT)
		
			var orden = datos[i].orden;
			var titulo = datos[i].titulo;
			var imagen = datos[i].imagen;
			var texto = datos[i].texto;
			var enlace = datos[i].enlace;
			var actividades = datos[i].actividades
			var avance = datos[i].avance
			var UT=datos[i].UT
			if(UT2!=UT)
			{	contenidoDiv += '<div class="item">';
				contenidoDiv += '    <div class="work">';
				contenidoDiv += '        <div class="img d-flex align-items-end justify-content-center" style="padding:5px;float:left;width:30%;height:200px;margin:5px;background-image: url(imagenes/imagen' + (i + 1) + '.jpg);">';
				contenidoDiv += '            <div class="" style="background-color:black;opacity: 0.;width:100%;padding:5px;">';
				contenidoDiv += '               <h5><a class="a_carousel" href="#UT'+(UT)+'">Unidad '+UT + ':' + texto + '</a></h5>';
				contenidoDiv += '           </div>';
				contenidoDiv += '        </div>';
				contenidoDiv += '    </div>';
				contenidoDiv += '</div>';
				UT2=UT;
			}


			
			
		//}
	}
	contenidoDiv += '</div>'
	document.getElementById('carousel').innerHTML += contenidoDiv;




	var fullHeight = function () {

		$('.js-fullheight').css('height', $(window).height());
		$(window).resize(function () {
			$('.js-fullheight').css('height', $(window).height());
		});

	};
	fullHeight();

	var carousel = function () {
		$('.featured-carousel').owlCarousel({
			loop: false,
			autoplay: false,
			margin: 30,
			animateOut: 'fadeOut',
			animateIn: 'fadeIn',
			nav: false,
			dots: false,
			autoplayHoverPause: false,
			items: 12,
			height:'200px',
			navText: ["<span class='ion-ios-arrow-back'></span>", "<span class='ion-ios-arrow-forward'></span>"],
			responsive: {
				0: {
					items: 1
				},
				600: {
					items: 1
				},
				1000: {
					items: 1
				}
			}
		});

	};
	carousel();
}
function drawActividades(datos) {

	var fecha = new Date('2023-10-01');

	// Obtener el valor del epoch en milisegundos
	var epoch = fecha.getTime();
	var unaSemana = 604800000;
	var FI = new Date(epoch + unaSemana);
	var FF = new Date(epoch + (2 * unaSemana));

	for (var i = 0; i < datos.length; i++) {
		var FI = new Date(epoch + (i * unaSemana));
		var FF = new Date(epoch + ((i + 1) * unaSemana));
		var dia = fecha.getDate();
		var mes = fecha.getMonth() + 1; // Los meses en JavaScript son base 0, se le suma 1
		var anio = fecha.getFullYear();
		var FIfechaFormateada = dia.toString().padStart(2, '0') + '/' + mes.toString().padStart(2, '0') + '/' + anio;

		var dia = fecha.getDate();
		var mes = fecha.getMonth() + 1; // Los meses en JavaScript son base 0, se le suma 1
		var anio = fecha.getFullYear();
		var FFfechaFormateada = dia.toString().padStart(2, '0') + '/' + mes.toString().padStart(2, '0') + '/' + anio;

		// Formatear la fecha como DD/MM/YYYY
		var fechaFormateada = dia.toString().padStart(2, '0') + '/' + mes.toString().padStart(2, '0') + '/' + anio;

		contenidoDiv = '<div id="UT' + (i + 1) + '"></div>';

		contenidoDiv += '<div class="container-xxl py-5 ">';
		if (i % 2 == 0) {
			contenidoDiv += '<div id="section"  style="background-color: rgba(0, 0, 0, 0.02);">';
		}
		else {
			contenidoDiv += '<div id="section"  style="background-color: rgba(255, 255, 255, 1);">';

		}
		contenidoDiv += '<div class="row">';
		contenidoDiv += '<div class="text-center wow fadeInUp" data-wow-delay="0.1s" style="visibility: visible; animation-delay: 0.1s; animation-name: fadeInUp;">';
		contenidoDiv += '<h6 class="section-title bg-white text-center text-primary px-3">Políticas de Marketing</h6>';
		contenidoDiv += '<h3 class="mb-5">' + datos[i].titulo + '</h3>';
		contenidoDiv += '</div>';

		contenidoDiv += '<div class="col-xs-10 col-md-8 col-lg-8 col-md-push-8 wow fadeInRight">';
		contenidoDiv += '<div style="width:100%;clear:both;margin-bottom:50px;background-color:gray;">';

		contenidoDiv += '<div style="float:left;width:40%;padding-left:20px;"><h6>Actividad</h6></div>';
		contenidoDiv += '<div style="float:left;width:15%;"><h6>Fecha inicio</h6></div>';
		contenidoDiv += '<div style="float:left;width:15%;"><h6>Fecha fin</h6></div>';
		contenidoDiv += '<div style="float:left;width:30%;"><h6>Acceder</h6></div>';
		contenidoDiv += '</div>';
		for (var k = 0; k < activPM.length; k++) {
			
			if (parseInt(activPM[k].UT) == i + 1) {
				contenidoDiv += '<div style="width:100%;clear:both;">';
				if (k == 0) {
					
				}
				contenidoDiv += '<div style="float:left;width:40%;">';
				contenidoDiv += '<img align="left" style="width:58px;margin:0px 10px 10px 10px;" src="imagenes/iconos/'
				
				
				if(activPM[k].tipo=='Simulación') {contenidoDiv+='Simulacion';}
				else if(activPM[k].tipo=='Desafío') {contenidoDiv+='Desafio';}
				else if(activPM[k].tipo=='Caso práctico') {contenidoDiv+='casopractico';}
				else
				{contenidoDiv += activPM[k].tipo;}
				
				
				contenidoDiv += '.svg" />';
				contenidoDiv += '<p>' + activPM[k].tipo + '';
				contenidoDiv += '<br>Apartados ' + activPM[k].contenido + ' del contenido</p>';
				contenidoDiv += '</div>';


				contenidoDiv += '<div style="float:left;width:15%;>';
				contenidoDiv += '<span "margin-left:20px;width:100px;" id="FI">' + FIfechaFormateada + '</span><span id="realizada"></span>';
				contenidoDiv += '</div>';
				contenidoDiv += '<div style="float:left;width:15%;>';
				contenidoDiv += '<span "margin-left:20px;width:100px;" id="FF">' + FFfechaFormateada + '</span><span id="realizada"></span>';
				contenidoDiv += '</div>';

				contenidoDiv += '<div style="float:left;width:15%;cursor:pointer;" id="' + activPM[k].id  + '" onclick="openActividad(this.id,0);">';
				contenidoDiv += '<img alt="Acceder como alumno" src="imagenes/iconos/boy-girl-icon.svg" style="width:32px;height:32px;padding-right:5px;" decoding="async" data-nimg="1" loading="lazy">';
				contenidoDiv += '</div>';

				contenidoDiv += '<div style="float:left;width:15%;cursor:pointer;" id="' + activPM[k].id + '" onclick="openActividad(this.id,1);">';
				contenidoDiv += '<img alt="Acceder como profesor" src="imagenes/iconos/training-icon.svg" style="width:32px;height:32px;padding-right:5px;" decoding="async" data-nimg="1" loading="lazy">';
				contenidoDiv += '</div>';
				contenidoDiv += '</div>';

				if (activPM[k].evaluacion==1)
				{
					textoIE += '<table width="100%"><tr><td width="20%">'+ k  +'</td><td width="50%">'+ activPM[k].tipo   +'</td><td>' +activPM[k].contenido   +'</td></tr></table>';
				};

			}
		};
		//tanco el primer bloc
		contenidoDiv += '</div>';
		textoIE ='<table width="100%"><tr style="font-weight:bold;"><td style="font-height:bold;"width="20%">Orden</td><td>Actividad</td><td>Puntos contenido</td></tr></table>'+textoIE
		contenidoDiv += '<div class="col-xs-10 col-md-3 col-lg-3 col-md-push-3 wow fadeInLeft ';
		contenidoDiv += 'accordion-boxed offset-top-0" role="tablist" aria-multiselectable="true" id="accordion-' + i + '">';
		apartado(i, 1, datos[i].tituloContenidos, datos[i].textoContenidos);
		apartado(i, 2, datos[i].tituloRA, datos[i].contenidoRA);
		apartado(i, 3, datos[i].tituloCE, datos[i].contenidoCE);
		apartado(i, 4, datos[i].tituloHR, datos[i].contenidoHR);
		apartado(i, 5, datos[i].tituloIE, textoIE);
		textoIE='';

		apartado(i, 6, datos[i].tituloCC, datos[i].contenidoCC);
		//tanco el segon bloc
		contenidoDiv += '</div>';
		//tanco el row
		contenidoDiv += '</div>';
		contenidoDiv += '</div>';
		contenidoDiv += '</div>';

		document.getElementById('modulos').innerHTML += contenidoDiv;

	}
}
function apartado(a, a2, b, c) {
	contenidoDiv += '<div class="card card-boxed" style="font-size:14px;"><a class="card-header collapsed" href="#" data-toggle="collapse" ';
	contenidoDiv += 'data-target="#accordion-' + a + '--card-' + a2 + '" ';
	contenidoDiv += 'aria-expanded="false"  ';
	contenidoDiv += 'aria-controls="accordion-' + a + '--card-' + a2 + '" style="font-size:14px;"> ';
	contenidoDiv += '<span class="fl-bigmug-line-checkmark14 icon-simulator novi-icon"></span>  ';
	contenidoDiv += b;
	contenidoDiv += '</a> ';
	contenidoDiv += '  <div class="collapse" id="accordion-' + a + '--card-' + a2 + '" aria-labelledby="headingOne" data-parent="#accordion-1" style=""> ';
	contenidoDiv += '    <div class="card-body"> ';
	contenidoDiv += '      <div class="row"> ';
	contenidoDiv += '        <div class="col-lg-12 col-md-pull-12-"> ';
	if(Array.isArray(c))
	{contenidoDiv += c.map(valor => `<p>${valor}</p>`).join(""); }
	else
	{contenidoDiv += c; }

	contenidoDiv += '      </div> ';
	contenidoDiv += '      </div> ';
	contenidoDiv += '    </div> ';
	contenidoDiv += '</div> ';
	contenidoDiv += '</div> ';

}
function ReadJson(archivo, funcion) {
	$.ajax({
		url: archivo,
		dataType: 'json',
		async: false,
		success: function (data) {
			// Manipula los datos del archivo JSON aquí
			eval(funcion);
		},
		error: function (xhr, status, error) {
			console.log('Error al cargar el archivo JSON');
		}
	});
}
function saveActividades(data) {
	activ = data;
}
function saveActividadesPM(data) {
	activPM = data;
}

function openActividad(a, tipo)
{
	//var divID = $(this).attr('id');

	var htmlURL='';
	if(tipo==0) {htmlURL = 'actividad.html?actividad='+a+'&rol=A'};
	if(tipo==1) {htmlURL = 'actividad.html?actividad='+a+'&rol=P'};

	window.open(htmlURL);
}
$('.ficha').each(function() {
	var divID = $(this).attr('id');
	var htmlURL = '../contenido/unidades/'+divID;

	// Cargar el HTML
	$(this).load(htmlURL);
});
