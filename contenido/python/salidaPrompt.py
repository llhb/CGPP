import openai
import json

openai.api_key = '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'  # Reemplaza 'TU_API_KEY' con tu clave de API de OpenAI

registroA = 5
salida = ''
# Carga los prompts desde el archivo 'prompts.txt'
with open('prompts'+ str(registroA) +'.txt', 'r') as file:
    prompts = file.read().splitlines()

print(prompts)
# Ejecuta los prompts y guarda las respuestas en un diccionario
responses = {}
for i, prompt in enumerate(prompts):
    response = openai.Completion.create(
        engine='text-davinci-003',
        prompt=prompt,
        max_tokens=1000,
        n=1,
        stop=None,  
        temperature=0.7
    )
    responses[str(i+1)] = response.choices[0].text.strip()

print(responses)
# Crea un nuevo diccionario con los números consecutivos como claves
for i, response in enumerate(responses.values()):
    if(i>1):
        print(i)
        print(response)
        salida= salida + "\n\n" + response

print(salida)
# Abre el archivo JSON y carga su contenido
with open('../../json/actividadesPM.json', 'r') as file:
    data = json.load(file)

print(data[registroA])
# Encuentra el registro con id=1 y actualiza su valor
for registro in data:
    if registro['id'] == registroA:
        registro['contenidoAI'] =  salida
        print(registro)


# Guarda los cambios en el archivo JSON
with open('../../json/actividadesPM.json', 'w') as file:
    json.dump(data, file, indent=4)