import openai

openai.api_key = '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'  # Reemplaza 'TU_API_KEY' con tu clave de API de OpenAI

prompts = [
    "Estoy desarrollando un curso sobre políticas de marketing para alumnos de 18 años. Te voy a pedir que actúes como un experto en contenidos para algunas actividades y como diseñador instruccional para otras.",
    "El temario del curso es el siguiente, no quiero que generes ahora ningún contenido. Simplemente tenlo en cuenta para no duplicar contenidos cuando solicite la realización de las actividades.1. La planificación de marketing.1.1. Concepto de plan de marketing. 1.2. Utilidades del plan de marketing. 1.3. Características del plan de marketing. 1.4. Estructura del plan de marketing.2. Evaluación de las oportunidades de mercado de una empresa. 2.1. Análisis de situación: externa e interna. 2.2. El DAFO. 2.3. La matriz MRG. 2.4. Selección de mercados objetivo.3. Determinación de las estrategias comerciales. 4. La segmentación del mercado. 4.1. Selección de mercados objetivo. 5. El marketing operativo. 5.1. Funciones del marketing operativo.",
    "Como experto en el tema, genera un contenido esquemático para el punto 1, en formato titular más ideas importantes sobre la planificación del marketing.",
    "Introduce dos ejemplos con casos reales que desarrollen las ideas más importantes sobre la planificación del marketing.",
    "El resultado de aprendizaje esperado es: Evalúa las oportunidades de mercado, para el lanzamiento de un producto, la entrada en nuevos mercados o la mejora del posicionamiento del producto o servicio, analizando las variables de marketing-mix  las tendencias y evolución del mercado. Céntrate en explicar cómo las ideas de este primer punto del temario nos permitirán alcanzar este resultado.",
    "La competencia que el alumno debe alcanzar es la de analizar las variables de marketing mix para conseguir los objetivos. Incluye una reflexión sobre esta competencia e introduce un ejemplo de cómo se demuestra esta competencia."
]

# Guarda los prompts en un archivo
with open('prompts.txt', 'w') as file:
    file.write(''.join(prompts))

print("Prompts guardados en el archivo prompts.txt")
