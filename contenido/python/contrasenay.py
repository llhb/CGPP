def encriptar_correo(correo):
    encriptado = ""
    for c in correo:
        encriptado += chr(ord(c) + 1)
    return encriptado

def desencriptar_correo(correo_encriptado):
    desencriptado = ""
    for c in correo_encriptado:
        desencriptado += chr(ord(c) - 1)
    return desencriptado

correo_original = "ejemplo@example.com"
correo_encriptado = encriptar_correo(correo_original)
print("Correo encriptado:", correo_encriptado)

correo_desencriptado = desencriptar_correo(correo_encriptado)
print("Correo desencriptado:", correo_desencriptado)
